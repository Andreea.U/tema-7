const fetchVerse = () => {
    const col = document.querySelector('.col');
    fetch('https://cors-anywhere.herokuapp.com/https://bible-api.com/Ioan+4:3?translation=rccv')
        .then(el => {
            return el.json();
        })
        .then(el => {
            console.log(el)
            col.innerHTML = el.text + el.reference;
        })
        .catch(error => console.error(error));
}
fetchVerse();